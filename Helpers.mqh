//+------------------------------------------------------------------+
//|                                                      Helpers.mqh |
//|                                                 Georgios Pierris |
//|                                                   www.pierris.gr |
//+------------------------------------------------------------------+

/*

The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.



*/



#property copyright "Georgios Pierris"
#property link      "www.pierris.gr"
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+


struct TechnicalIndicator{

   string name;
   double value[];
   
};

string subfolder = "data";


int MA_Period = 8; //Moving Average Period
int adx_period = 8;

//Initializations for iStochastic and myStochasticJ
int                  Kperiod=5;                 // the K period (the number of bars for calculation)
int                  Dperiod=3;                 // the D period (the period of primary smoothing)
int                  slowing=3;                 // period of final smoothing      
ENUM_MA_METHOD       ma_method=MODE_SMA;        // type of smoothing   
ENUM_STO_PRICE       price_field=STO_LOWHIGH;   // method of calculation of the Stochastic

int                  stochasticJperiod = 5;

int                  iwprPeriod = 14;

int                  irsiPeriod = 14;


// Takes an array of values
int GetIndicatorsForPeriod(string &techIndNames[], datetime startDay, datetime endDay, TechnicalIndicator &techIndicators[]){

   //Initializations
   
   int curIndicator = 0;
   for(int i =0; i < ArraySize(techIndNames); i++){
      
      //Take the current index which should increase...
      curIndicator = ArraySize(techIndicators);
      ArrayResize(techIndicators, curIndicator+1);
      
      string ind =  techIndNames[i];
      techIndicators[curIndicator].name = ind; 
      SetIndexBuffer(i, techIndicators[curIndicator].value, INDICATOR_DATA);


      int _aHandle = -1; 
      int _bHandle = -1;
   
      

///////////////  BEGIN OF CHOICES   /////////////////////////////////////

      if(StringCompare(ind, "iAC") == 0){
        //Get the handle for the Accelerator Oscillator indicator
         _aHandle = iAC(_Symbol, _Period); 
      }
      else if(StringCompare(ind, "iAD") == 0){
         _aHandle = iAD(_Symbol, _Period, VOLUME_TICK); // Or VOLUME_TICK?
      }
      else if(StringCompare(ind, "iADX") == 0){
         _aHandle = iADX(_Symbol, _Period, adx_period);
      }
      else if(StringCompare(ind, "iADXWilder") == 0){
         _aHandle = iADXWilder(_Symbol, _Period, adx_period);
      }
      else if(StringCompare(ind, "iAligator") == 0){
         continue;//_aHandle = i
      }
      else if(StringCompare(ind, "iAMA") == 0){
         continue;//_aHandle = i
      }
      else if(StringCompare(ind, "iAO") == 0){
         _aHandle = iAO(_Symbol, _Period);
      }
      else if(StringCompare(ind, "iMA") == 0){
         //Get the handle for Moving Average indicator
         _aHandle = iMA(_Symbol, _Period, MA_Period, 0, MODE_EMA, PRICE_CLOSE);
      }
      else if(StringCompare(ind, "MAShortMid") == 0 || StringCompare(ind, "MAShortLong") == 0 || StringCompare(ind, "MAMidLong") == 0){
         
         int MA1=0, MA2=0;
         double temp[];
         
         //FIXME!!! CANNOT HAVE MULTIPLE different periods....See below
         if(StringCompare(ind, "MAShortMid") == 0) { MA1 = 13; MA2 = 55;   }
         if(StringCompare(ind, "MAShortLong") == 0){ MA1 = 13; MA2 = 144;  }
         if(StringCompare(ind, "MAMidLong") == 0)  { MA1 = 55; MA2 = 144;  }
         
         //if( !IndicatorRelease(_aHandle) ) {Alert("Could not release indicator 1!");};
         //if( !IndicatorRelease(_bHandle) ) {Alert("Could not release indicator 2!");};
         
         _aHandle = iMA(_Symbol, _Period, MA1, 0, MODE_EMA, PRICE_CLOSE);
         _bHandle = iMA(_Symbol, _Period, MA2, 0, MODE_EMA, PRICE_CLOSE);
         
         if(_aHandle < 0 || _bHandle < 0){
            Alert("Error Creating the ", ind, " handle! Error: ", GetLastError());
            return(-1);
         }
         ResetLastError();
         while(BarsCalculated(_aHandle) < 0 && BarsCalculated(_bHandle) < 0){
            Sleep(50);
         }
         
         int copied1 = CopyBuffer(_aHandle, 0, startDay, endDay, techIndicators[curIndicator].value);
         int copied2 = CopyBuffer(_bHandle, 0, startDay, endDay, temp);
         
         if(copied1 < 0){
            Print(ind, " : Error copying data 1, error: ", GetLastError());
            return(-1);
         }
         
         if(copied2 < 0){
            Print(ind, " : Error copying data 2, error: ", GetLastError());
            return(-1);
         }
         ResetLastError();
         if( !IndicatorRelease(_aHandle) ) {Alert("Could not release indicator 1! error:  ", GetLastError());};
         if( !IndicatorRelease(_bHandle) ) {Alert("Could not release indicator 2! error:  ", GetLastError());};
          
         //Take the ratio
         for(int elem = 0; elem < ArraySize(temp); elem++){
            techIndicators[curIndicator].value[elem] = techIndicators[curIndicator].value[elem]/temp[elem];
         }
         
         continue; //Will have to avoid the the copying again and release of indicator below for the general case!!!
         
      }
      else if(StringCompare(ind, "iStochasticK") == 0 || StringCompare(ind, "iStochasticD") == 0){
         

         _aHandle = iStochastic(_Symbol, _Period, Kperiod, Dperiod, slowing, ma_method, price_field);
         
         ResetLastError();
          while(BarsCalculated(_aHandle) < 0){
            Sleep(50);
         }
   
         if(StringCompare(ind, "iStochasticK") == 0){
            int copied = CopyBuffer(_aHandle, MAIN_LINE, startDay, endDay, techIndicators[curIndicator].value);
            if(copied < 0){
               Alert("StochasticK: Error copying data, error: ", GetLastError());
               return(-1);
            } 
         
         }
         else{
            int copied = CopyBuffer(_aHandle, SIGNAL_LINE, startDay, endDay, techIndicators[curIndicator].value);
            if(copied < 0){
               Alert("StochasticD: Error copying data, error: ", GetLastError());
               return(-1);
            } 
         
         }
         
         if( !IndicatorRelease(_aHandle) ) {Alert("Could not release indicator 3!");};
        
         continue; //Will have to avoid the the copying again and release of indicator below for the general case!!!
         
      }
      else if( StringCompare(ind, "myStochasticJ") == 0){
         
         _aHandle = iStochastic(_Symbol, _Period, Kperiod, Dperiod, slowing, ma_method, price_field);
         //FIXME!!! FIND HOW TO DO IT WITH iMA on the handle AND NOT MANUALLY
         //We should do something like that, however, this currently calculates the moving average of %K, i.e., %D. We actually want 
         //the moving average of the signal_line which we currently do manually
         /*
         int _bHandle = iMA(_Symbol, _Period, stochasticJperiod, 0, ma_method,_aHandle);
         ResetLastError();
         int copied = CopyBuffer(_bHandle, MAIN_LINE, startDay, endDay, techIndicators[curIndicator].value);
         if(copied < 0){
            Alert("MyStochasticJ: Error copying data, error: ", GetLastError());
            return(-1);
         }
         
         IndicatorRelease(_aHandle);
         IndicatorRelease(_bHandle);
         */
         //Here we take the %D and then we will calculate the moving average of the signal
         
         ResetLastError();
         while(BarsCalculated(_aHandle) < 0){
            Sleep(50);
         }
   
         
         int copied = CopyBuffer(_aHandle, SIGNAL_LINE, startDay, endDay, techIndicators[curIndicator].value);
         if(copied < 0){
            Alert("MyStochasticJ: Error copying data, error: ", GetLastError());
            return(-1);
         }
         
         //My custom moving average
         double sum;
         for(int elem = stochasticJperiod-1; elem<ArraySize(techIndicators[curIndicator].value); elem++){
            sum = 0.0;
            for(int j=0; j<stochasticJperiod; j++){
               sum += techIndicators[curIndicator].value[elem-j]; 
            }
            techIndicators[curIndicator].value[elem] = sum/((double) stochasticJperiod);
         }
         
         if( !IndicatorRelease(_aHandle) ) {Alert("Could not release indicator 4!");};
         
         continue; //Will have to avoid the the copying again and release of indicator below for the general case!!!
   
      }
      else if(StringCompare(ind, "iWPR") == 0){
         _aHandle = iWPR(_Symbol, _Period, iwprPeriod);
      }
      else if(StringCompare(ind, "iOBV") == 0){
         _aHandle = iOBV(_Symbol, _Period, VOLUME_TICK);
      }
      else if(StringCompare(ind, "iRSI") == 0){
         _aHandle = iRSI(_Symbol, _Period, irsiPeriod, PRICE_CLOSE);
      }
      else{   
            Alert(ind," : Invalid technical indicator!");
      }
 
 
 
 //////////////   END OF CHOICES  ///////////////////////
 
      if(_aHandle < 0){
         Alert("Error Creating the ", ind, " Handle! Error: ", GetLastError());
         return(-1);
      }

      ResetLastError();
      while(BarsCalculated(_aHandle) < 0){
         Sleep(50);
      }
      int copied = CopyBuffer(_aHandle, 0, startDay, endDay, techIndicators[curIndicator].value);

      if(copied < 0){
         Alert(ind, " : Error copying data, error: ", GetLastError());
         return(-1);
      }
      
      if( !IndicatorRelease(_aHandle)) {Alert("Could not release indicator 5!");};
   
   }

return(1);
}


int ReadTechnicalIndicatorsList(string &names[]){

   string temp[] = {
      "iAC", 
      "iAD",
      "iADX",
      "iADXWilder",
      //"iAO",
      //"MAShortMid",
      "MAShortLong",
      //"MAMidLong",
      //"iStochasticK",
      "iStochasticD"
      //"myStochasticJ", /*Moving average of stochastic %D*/ //FIXME!!! produces error
      //"iWPR",
      //"iOBV",
      //"iRSI"
      };
 
   ArrayCopy(names, temp);
   
   return (1);
}


int submitRequestForPrediction(long counter, int nBarsInPast){
   
   datetime times[], startPos, endPos;
   MqlRates myrates[];
   
   int copied = CopyTime(_Symbol, _Period, 0, nBarsInPast, times);
   if(copied>0){ //Succesfully copied
      endPos = times[0]; //The first is the starting position 
      startPos = times[ArraySize(times)-1];  //and the last is the ending in the past
   }
   else{
      Alert("Could not copy times!");
      return(-1);
   }
  
   
   string techIndNames[];
   ReadTechnicalIndicatorsList(techIndNames);
   
   TechnicalIndicator myIndicators[];
   //Alert("Times... :", TimeToString(startPos)," - ", TimeToString(endPos));
   int ret = GetIndicatorsForPeriod(techIndNames, startPos, endPos, myIndicators);
   if(ret == -1){
      Alert("Error reading technical indicators");
      return(-1);
   }
   //Get the details of the latest nBars
   
   if(CopyRates(_Symbol, _Period, startPos, endPos, myrates) < 0){
      Alert("Error copying rates/history data, error: ", GetLastError());
      return(-1);
   }
  
   string fileName = subfolder + "\\predictionRequest_"+IntegerToString(counter)+".txt";
 
   int bytesWritten = FileWriteRatesAndTechnicalIndicators(fileName, myrates, myIndicators);
 
   if( bytesWritten == -1){
      Alert("Error submitting prediction!");
      return(-1);
   }
   
   //Print("bytes: ", bytesWritten);
    
   
   
   return(1);
}

//FILE_COMMON allows a shared folder between agents to write files 
//Files are written in the C:\ProgramData\MetaQuotes\Terminal\Common\Files\data
int FileWriteRatesAndTechnicalIndicators(string fileName, MqlRates &myrates[], TechnicalIndicator &indicators[]){
   
   uint bytesWritten = 0;
   //Print("Path: ", TerminalInfoString(TERMINAL_COMMONDATA_PATH), "filename: ", fileName);
   
   int fileHandle = FileOpen(fileName, FILE_WRITE | FILE_CSV | FILE_COMMON, ',');
   
   if(fileHandle != INVALID_HANDLE){
      ;//Print("The file has been successfully opened!!!");
   }
   else{
      Print("File open failed, error: ", GetLastError());
      return(-1);
   }
 
  
   //Print(ArraySize(myrates));
   for(int i = 0 ; i < ArraySize(myrates); i++){
      bytesWritten += FileWrite(fileHandle, myrates[i].time, 
      
      // CHOOSE ONLY THOSE THAT ARE NEEDED!!!
                            //myrates[i].open, 
                            myrates[i].close, 
                            //myrates[i].real_volume, 
                            //myrates[i].tick_volume, 
                            myrates[i].spread,
                            "");
                            
      //Add indicators in the feature set
      int numOfIndicators = ArraySize(indicators);
      for(int j = 0; j < numOfIndicators; j++){
        
         //FIXME: There maybe problem with the 32bit machines!!! and the -4 bytes...
         FileSeek(fileHandle, -4, SEEK_CUR);
         //This is only to verify
         if(j == numOfIndicators-1){
            bytesWritten += FileWrite(fileHandle, indicators[j].value[i]);
         }
         else{
            bytesWritten += FileWrite(fileHandle, indicators[j].value[i], "");
         }
      
      }
   }
   
   FileClose(fileHandle);
   return (int)bytesWritten;
 }


int readPrediction(long counter){
   
   int prediction;
   int waitTime = 100000000; //milliseconds
   int sleepTime = 100;
  
   string predictionFilename = "prediction_" + IntegerToString(counter) + ".txt";
   //Print(predictionFilename);
   
   int fileHandle = 0;
   
   while( waitTime > 0 ){
      
      fileHandle = FileOpen(predictionFilename, FILE_CSV|FILE_ANSI|FILE_READ);
         
      if(fileHandle != INVALID_HANDLE){
         //Print("The prediction file has been successfully opened");
         break;
      }
      else{
         //Print("Prediction file open failed, error: ", GetLastError());
      }
    
      //Print("Waiting for prediction... Time: ", waitTime, " counter: ", counter ," FileName: ", predictionFilename);
      //ResetLastError();
      //Sleep(sleepTime); //sleep for some milliseconds...
      //Print("After sleep: ", GetLastError());
      waitTime = waitTime - sleepTime;
   }
   
   if(waitTime <= 0 ){
   
      Print("Warning: Timeout waiting for prediction ");
      return(-1);
   
   }
  
   //Print("Path: ", TerminalInfoString(TERMINAL_COMMONDATA_PATH));
   //Print("Path: ", TerminalInfoString(TERMINAL_DATA_PATH));
   //Print("Path: ", TerminalInfoString(TERMINAL_PATH));
   
  
   
   prediction = StringToInteger(FileReadString(fileHandle, 3));
   Print("My prediction is ", prediction);
   
   FileClose(fileHandle);
   
   return prediction;
  
  
}
