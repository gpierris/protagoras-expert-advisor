# Protagoras Expert Advisor

##Before You Start

Learn a few things about [Protagoras](http://en.wikipedia.org/wiki/Protagoras), that amazing sophist and his famous quote "Man is the measure of all things".

## The Cool Stuff Now

**Protagoras has shown some interesting results for daily stock market predictions, however, in Forex exchange and intra-day trading it is not ready yet.

This serves only as an experimental interface to allow for creating MQL 5 Expert Advisors for Metatrader and interact with the Protagoras Learning framework. See the other repository.

This work is by no means mature enough, however, I distribute it in case anyone finds it useful or interesting, before I find the time to commit more often.

I assume that you should know your way around with MQL 5; however, if there is any question, please find my contact information at my [personal website](http://www.pierris.gr)
   
## Getting Started

There are pretty much 3 files you need:

* GetHistoricalData.mq5

It will bring back the data for a specified date range, and the resolution will be dependend on where it is attached, e.g., M1.

```
    ...
    input datetime StartDay=D'2013.05.01 00:00:00';
    input datetime EndDay=D'2013.06.01 00:00:00';
    ...
```

It will save historical data to a file in the common folder, e.g. ```C:\ProgramData\MetaQuotes\Terminal\Common\Files\data\HistoricalData_2013.05.01_To_2013.06.01``` , but what data?

That will be timestamps and MQL rates followed by a list of indicators you would like to have. See Helpers.mqh.

* Helpers.mqh

At ReadTechnicalIndicatorsList you will find a list of technical indicators that MQL is providing, so add or remove accordingly in the list.

A lot of other cool stuff are happening in this header....

* EAPredictor.mq5

Under Construction...

## Academics trying Quantitave Trading

-![alt text](http://gifs.gifbin.com/102012/1350322998_girl_vs_pinata_fail.gif "" README.md URL")