//+------------------------------------------------------------------+
//|                                            GetHistoricalData.mq5 |
//|                                                 Georgios Pierris |
//|                                                   www.pierris.gr |
//+------------------------------------------------------------------+

/*

The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/



#include "Helpers.mqh"

#property copyright "Georgios Pierris"
#property link      "www.pierris.gr"
#property version   "1.00"
//--- input parameters
input datetime StartDay=D'2013.05.01 00:00:00';
input datetime EndDay=D'2013.06.01 00:00:00';


MqlRates mrates[];


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
//int OnInit()
int OnStart() //That should be OnInit
  {
//---
   ResetLastError();
   
   string techIndNames[];
   ReadTechnicalIndicatorsList(techIndNames);
   
   TechnicalIndicator myIndicators[];
   int ret = GetIndicatorsForPeriod(techIndNames, StartDay, EndDay, myIndicators);
   if(ret == -1){
      Alert("Error reading technical indicators");
      return(-1);
   }
   //Get the details of the latest nBars
   
   if(CopyRates(_Symbol, _Period, StartDay, EndDay, mrates) < 0){
      Alert("Error copying rates/history data, error: ", GetLastError());
      return(-1);
   }

   string fileName = subfolder + "\\HistoricalData_" + TimeToString(StartDay, TIME_DATE) + "_To_" + TimeToString(EndDay, TIME_DATE) + ".txt";   
   FileWriteRatesAndTechnicalIndicators(fileName, mrates, myIndicators);

  
   Print("The file has been successfully closed");
//---
   return(0);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//---
   
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   
  }
//+------------------------------------------------------------------+
