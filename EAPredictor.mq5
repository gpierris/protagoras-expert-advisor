//+------------------------------------------------------------------+
//|                                                  EAPredictor.mq5 |
//|                                                 Georgios Pierris |
//|                                                   www.pierris.gr |
//+------------------------------------------------------------------+

/*

The MIT License (MIT)

Copyright (c) 2013 Georgios Pierris

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/




#property copyright "Georgios Pierris"
#property link      "www.pierris.gr"
#property version   "1.00"

#include "Helpers.mqh"


//--- input parameters
input int      pastBars = 1;    //The number of past bars we will consider for our prediction
input int      StopLoss=30;      //Stop Loss
input int      TakeProfit=100;   //Take Profit
input int      ADX_Period=8;     //ADX Period
//input int      MA_Period=8;      //Moving Average Period
input int      EA_Magic=12345;    //EA Magic Number
input double   Adx_Min=22.0;     //Minimum ADX Value
input double   Lot=0.1;          //Lots to trade

//Other parameters

int adxHandle;                      //Handle for our ADX indicator
int maHandle;                       //Handle for our Moving Average indicator
double plsDI[], minDI[], adxVal[];  //Dynamic arrays to hold the values of +DI, -DI and ADX values...
double maVal[];                     //Dynamic array to store the close value of a bar
double p_close;                     //Variable to store the close value of a bar
int STP, TKP;                       //To be used for Stop Loss & Take Profit values

long predictionCounter = 0;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
  {
//---
   //Get the handle for ADX indicator
   adxHandle = iADX(NULL, 0, ADX_Period);
   
   //Get the handle for Moving Average indicator
   maHandle = iMA(_Symbol, _Period, MA_Period, 0, MODE_EMA, PRICE_CLOSE);
   
   if(adxHandle < 0 || maHandle < 0){
      Alert("Error Creating Handles for indicators! Error: ", GetLastError());
   }
   
   //--- Let us handle currency pairs with 5 or 3 digit prices instead of 4
   STP = StopLoss;
   TKP = TakeProfit;
/*   if(_Digits==5 || _Digits==3)
   {
      STP = STP*10;
      TKP = TKP*10;
   }
  */ 
//FIXME: Shouldn't be here... Python will create this file...
 /*  string predictionFilename = "prediction_0"  + ".txt";
   int fileHandle = FileOpen(predictionFilename, FILE_CSV|FILE_ANSI|FILE_WRITE);
         
      if(fileHandle != INVALID_HANDLE){
         Print("The prediction file has been successfully opened");
      }
      else{
         Print("Prediction file open failed, error: ", GetLastError());
      }
    FileWrite(fileHandle, "0");
    FileClose(fileHandle);
*/
//---
   return(0);
  }
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
  {
//--
   IndicatorRelease(adxHandle);
   IndicatorRelease(maHandle);
  }
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
void OnTick()
  {
//---
   
   //Do we have enough bars to work with?
   if(Bars(_Symbol, _Period) < pastBars){
      Alert("We have only ", Bars(_Symbol, _Period), " bars. Waiting until we have ", pastBars, " !");
      return;
   }
   
   static datetime Old_Time;
   datetime New_Time[1];
   bool IsNewBar = false;
   
   int copied = CopyTime(_Symbol, _Period, 0, 1, New_Time);
   if(copied>0){ //Succesfully copied
   
      if(Old_Time != New_Time[0]){
         
         IsNewBar = true;
         
         if(MQL5InfoInteger(MQL5_DEBUGGING)){
            Print("We have a new bar here ", New_Time[0], ", old time was ", Old_Time); 
         }
         Old_Time = New_Time[0];
         
      }
   
   }
   else{
   
      Alert("Error in copying historical times data, error: ", GetLastError());
      ResetLastError();
      return;
   
   }
   
   if(IsNewBar == false){
      return;
   }
   
   //Verify again that we have enough bars to work with
   int myBars = Bars(_Symbol, _Period);
   if(myBars < pastBars){
      Alert("We have only ", myBars, " bars. Waiting until we have ", pastBars, " !");
      return;
   }
   
   //Define some MQL structures we will use for our trade
   
   MqlTick latestPrice;          //To be used for getting recent/latest price quotes
   MqlTradeRequest mrequest;     //To be used for sending our trade request
   MqlTradeResult mresult;     //To be used to store the prices, volumes, and spread of each bar
   MqlRates mrate[];             //To be used to store the prices volumes and spread of each bar
   ZeroMemory(mrequest);         //Initialization of mrequest structure
   ZeroMemory(mresult);
   
   //Make sure Rates, ADX Values and MA values are stored serially similar to the timeseries array
   
   ArraySetAsSeries(mrate, true);
   ArraySetAsSeries(plsDI, true);
   ArraySetAsSeries(minDI, true);
   ArraySetAsSeries(adxVal, true);
   ArraySetAsSeries(maVal, true);
   
   
   //Get latest price quote using the MQL5 MqlTick Structure
   if(!SymbolInfoTick(_Symbol, latestPrice)){
      Alert("Error getting the latest price quote, error: ", GetLastError());
      return;
   }
   
   
   //Get the details of the latest nBars
   int nBars = 3;
   if(CopyRates(_Symbol, _Period, 0, nBars, mrate) < 0){
      Alert("Error copying rates/history data, error: ", GetLastError());
      return;
   }
   
   //Copy the new values of our indicators to buffers (arrays) using the handle
   
   if(CopyBuffer(adxHandle, 0, 0, nBars, adxVal) < 0 ||
      CopyBuffer(adxHandle, 1, 0, nBars, plsDI) < 0 ||
      CopyBuffer(adxHandle, 2, 0, nBars, minDI) < 0){
   
      Alert("Error copying ADX indicator Buffers, error: ", GetLastError());
      return;
   
   }
  
   if(CopyBuffer(maHandle, 0, 0, nBars, maVal) < 0){
      Alert("Error copying Mean Average indicator Buffer, error: ", GetLastError());
      return;
   }
   
   bool buyOpened = false; //variable to hold the result of Buy opened position
   bool sellOpened = false; //variable to hold the result of Sell opened position
   
   if(PositionSelect(_Symbol) == true){ //we have opened a position
      
      if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_BUY){
         buyOpened  = true; //It is a buy
      }
      
      else if(PositionGetInteger(POSITION_TYPE) == POSITION_TYPE_SELL){
         sellOpened = true;
      }
   }
   
   
   /* Check for long/Buy Setup: MA-8 increasing upwards, previous price close above it, ADX > 22, +DI > -DI */
   
   bool BuyCondition1 = (maVal[0] > maVal[1]) && (maVal[1]>maVal[2]); //MA-8 increasing upwards
   bool BuyCondition2 = (adxVal[0] > Adx_Min);  //Current ADX value greater than minimum
   bool BuyCondition3 = (plsDI[0] > minDI[0]);  //+DI > -DI

   bool SellCondition1 = (maVal[0] < maVal[1]) && (maVal[1] < maVal[2]); //MA-8 increasing upwards
   bool SellCondition2 = (adxVal[0] > Adx_Min);  //Current ADX value greater than minimum
   bool SellCondition3 = (plsDI[0] < minDI[0]);  //+DI > -DI

   int prediction = 0; // 1 for buy, -1 for sell, 0 no operation

   if(( BuyCondition1 && BuyCondition2 && BuyCondition3) || (SellCondition1 && SellCondition2 && SellCondition3)){
      
      submitRequestForPrediction(predictionCounter, pastBars); //Saves a file with the feature vector
      //Print("predictionCounter: ", predictionCounter);
      prediction = readPrediction(predictionCounter); //Reads from another file the prediction

   }
   
   if(prediction == 1){
   
         if(buyOpened){
            Alert("We already have a Buy Position!");
            return;
         }
         
         mrequest.action = TRADE_ACTION_DEAL;
         mrequest.price = NormalizeDouble(latestPrice.ask, _Digits);
         mrequest.sl = NormalizeDouble(latestPrice.ask - STP*_Point, _Digits);
         mrequest.tp = NormalizeDouble(latestPrice.ask + TKP*_Point, _Digits);
         mrequest.symbol = _Symbol;
         mrequest.volume = Lot;
         mrequest.magic = EA_Magic;
         mrequest.type = ORDER_TYPE_BUY;
         mrequest.type_filling = ORDER_FILLING_FOK;
         mrequest.deviation = 100;
   
         Print("Request: sl: ", mrequest.sl, " and tp: ", mrequest.tp);
         
         //Send Order
         OrderSend(mrequest, mresult);
      
         //get the result code
         if(mresult.retcode == 10009 || mresult.retcode == 10008){
         
            Alert("A Buy order has been successfully placed with Ticket#: ", mresult.order);
         }
         else{
            
            Alert("The Buy order request could not be completed, error: ", GetLastError());
            return;
   
         }
         
         predictionCounter++;
         
   }
   else if(prediction == -1){
   
         if(sellOpened){
            Alert("We already have a Sell Position!");
            return;
         }
         
         mrequest.action = TRADE_ACTION_DEAL;
         mrequest.price = NormalizeDouble(latestPrice.bid, _Digits);
         mrequest.sl = NormalizeDouble(latestPrice.bid + STP*_Point, _Digits);
         mrequest.tp = NormalizeDouble(latestPrice.bid - TKP*_Point, _Digits);
         mrequest.symbol = _Symbol;
         mrequest.volume = Lot;
         mrequest.magic = EA_Magic;
         mrequest.type = ORDER_TYPE_SELL;
         mrequest.type_filling = ORDER_FILLING_FOK;
         mrequest.deviation = 100;
 
          Print("Request: sl: ", mrequest.sl, " and tp: ", mrequest.tp);
         
         //Send Order
         OrderSend(mrequest, mresult);
      
         
         if(mresult.retcode == 10009 || mresult.retcode == 10008){
            Alert("A Sell order has been successfully placed with Ticket#: ", mresult.order);
         }
         else{
            Alert("The Sell order request could not be completed, error: ", GetLastError());
            ResetLastError();
            return;
         }
         predictionCounter++;
     }

   return;

}
   
   

//+------------------------------------------------------------------+
